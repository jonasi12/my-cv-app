package com.example.jonathan.myapplication.activities;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.example.jonathan.myapplication.R;
import com.example.jonathan.myapplication.helpers.ToolBarHelper;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by jonathan.asilo on 9/16/2016.
 */

public class ContactMeActivity extends AppCompatActivity{

    @BindView(R.id.toolbar)
    Toolbar mToolBar;

    @BindView(R.id.tvLinkedIn)
    TextView mTvLinkedIn;

    @BindView(R.id.tvWantedly)
    TextView mTvWantedly;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.anim_fade_in, R.anim.anim_fade_out);
        setContentView(R.layout.activity_contact_me);
        ButterKnife.bind(this);
        setSupportActionBar(new ToolBarHelper(this).initToolBar(mToolBar, "Contact Me"));
    }

    @Override
    protected void onPause() {
        super.onPause();
        overridePendingTransition(R.anim.anim_fade_in, R.anim.anim_fade_out);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            this.finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @OnClick({R.id.tvLinkedIn, R.id.tvWantedly})
    public void onTextViewClicked(View v) {
        switch (v.getId()){
            case R.id.tvLinkedIn:
                startActivity(new Intent(Intent.ACTION_VIEW).setData(Uri.parse(mTvLinkedIn.getText().toString())));
                break;

            case R.id.tvWantedly:
                startActivity(new Intent(Intent.ACTION_VIEW).setData(Uri.parse(mTvWantedly.getText().toString())));
                break;
        }
    }
}
