package com.example.jonathan.myapplication.fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.jonathan.myapplication.R;
import com.example.jonathan.myapplication.adapters.PortfolioListAdapter;
import com.example.jonathan.myapplication.data.PortfolioData;
import com.example.jonathan.myapplication.models.Portfolio;

import com.github.florent37.materialviewpager.MaterialViewPagerHelper;
import com.github.florent37.materialviewpager.adapter.RecyclerViewMaterialAdapter;


import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by jonathan.asilo on 9/16/2016.
 */

public class PortfolioFragment extends Fragment {

    @BindView(R.id.rvPortfolio)
    RecyclerView mRvPortfolio;

    PortfolioClickListener mPortfolioClickListener;

    public interface PortfolioClickListener {
        void onClickPortfolio(View v);
    }

    public PortfolioFragment(){

    }

    public static PortfolioFragment newInstance() {
        return new PortfolioFragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_portfolio, container, false);
        ButterKnife.bind(this, view);

        loadData();

        return view;
    }

    private void loadData(){
        PortfolioListAdapter portfolioListAdapter = new PortfolioListAdapter(PortfolioData.returnData(getActivity()));
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());

        mRvPortfolio.setLayoutManager(layoutManager);
        mRvPortfolio.setAdapter(new RecyclerViewMaterialAdapter(portfolioListAdapter));
        mRvPortfolio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPortfolioClickListener.onClickPortfolio(v);
            }
        });

        registerRecycler();
    }

    private void registerRecycler(){
        MaterialViewPagerHelper.registerRecyclerView(getActivity(), mRvPortfolio);
    }
}
