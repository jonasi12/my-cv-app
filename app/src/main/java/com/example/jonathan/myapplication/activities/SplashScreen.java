package com.example.jonathan.myapplication.activities;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.example.jonathan.myapplication.R;

/**
 * Created by jonathan.asilo on 9/16/2016.
 */

public class SplashScreen extends AppCompatActivity {

    // Splash screen timer
    private static int SPLASH_TIME_OUT = 500;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.anim_fade_in, R.anim.anim_fade_out); // transition effet
        setContentView(R.layout.activity_splash_screen);

        new Handler().postDelayed(new Runnable() { //delay run
            @Override
            public void run() {

                startActivity(new Intent(SplashScreen.this, MainActivity.class)); // go to main activity
                finish();
            }
        }, SPLASH_TIME_OUT);

    }

    @Override
    protected void onPause() {
        super.onPause();
        overridePendingTransition(R.anim.anim_fade_in, R.anim.anim_fade_out);
    }
}
