package com.example.jonathan.myapplication.data;

import android.content.Context;
import android.graphics.BitmapFactory;

import com.example.jonathan.myapplication.R;
import com.example.jonathan.myapplication.models.Portfolio;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jonathan.asilo on 9/16/2016.
 */

public class PortfolioData {

    public static List<Portfolio> returnData(Context context){
        List<Portfolio> portfolioList = new ArrayList<>();

        Portfolio portfolioData = new Portfolio();
        portfolioData.setImage(BitmapFactory.decodeResource(context.getResources(),
                                R.drawable.logo_dj));
        portfolioData.setTitle("Dirty Jobs App");
        portfolioData.setDescription("DirtyJobsPH App offers day-to-day household and specific services through an online platform, by partner service providers, to individuals and families needing them. Along with the PARTNER SERVICE PROVIDERS, DirtyJobs.ph aims to provide quality service to its users through the unique and reliable services offered by the partners.");
        portfolioList.add(portfolioData);

        Portfolio portfolioData2 = new Portfolio();
        portfolioData2.setImage(BitmapFactory.decodeResource(context.getResources(),
                                R.drawable.logo_flawless));
        portfolioData2.setTitle("Flawless App");
        portfolioData2.setDescription("Make your beauty routine fun and easy in just a few taps. Brought to you by Flawless, the Philippines’ most preferred beauty clinic, #stayFlawless app gives the best aesthetic experience in your hands.");
        portfolioList.add(portfolioData2);

        Portfolio portfolioData3 = new Portfolio();
        portfolioData3.setImage(BitmapFactory.decodeResource(context.getResources(),
                                R.drawable.logo_viz));
        portfolioData3.setTitle("Vizcarra App");
        portfolioData3.setDescription("Vizcarra Checkinout is an employee management and accounting system that uses the mobile and cloud technology for fast results. The app records the attendance of employees. This app is exclusive per company only and its employees.");
        portfolioList.add(portfolioData3);

        Portfolio portfolioData4 = new Portfolio();
        portfolioData4.setImage(BitmapFactory.decodeResource(context.getResources(),
                                R.drawable.logo_tapdash));
        portfolioData4.setTitle("Tapdash Heuristics App");
        portfolioData4.setDescription("CheckIn-Out is an employee management and accounting system that uses the mobile and cloud technology for fast results. The app records the attendance of employees and computes payroll automatically. This app is exclusive per company only and its employees.");

        portfolioList.add(portfolioData4);
        return portfolioList;
    }
}
