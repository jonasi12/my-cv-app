package com.example.jonathan.myapplication.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import com.example.jonathan.myapplication.R;
import com.example.jonathan.myapplication.interfaces.LoadFragment;
import com.github.florent37.materialviewpager.MaterialViewPagerHelper;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollView;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by jonathan.asilo on 9/16/2016.
 */
public class AmbitionFragment extends Fragment implements LoadFragment {

    @BindView(R.id.svAmbition)
    ObservableScrollView mSvAmbition;

    @BindView(R.id.tvAmbition)
    TextView mTvAmbition;

    Animation mAnimFadeIn;

    public AmbitionFragment() {
        // Required empty public constructor
    }

    public static AmbitionFragment newInstance(){
        return new AmbitionFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_ambition, container, false);
        ButterKnife.bind(this, view);
        initViews(view);
        return view;
    }

    private void initViews(View v){
        mAnimFadeIn = AnimationUtils.loadAnimation(getActivity(), R.anim.anim_fade_in);
        mTvAmbition.startAnimation(mAnimFadeIn);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        MaterialViewPagerHelper.registerScrollView(getActivity(), mSvAmbition, null);

    }

    @Override
    public void onLoadFragment() {

    }
}
