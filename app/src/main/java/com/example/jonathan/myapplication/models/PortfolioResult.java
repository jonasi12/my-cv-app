package com.example.jonathan.myapplication.models;

/**
 * Created by jonathan.asilo on 7/11/2016.
 */

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by jonathan.asilo on 9/16/2016.
 */

public class PortfolioResult {

    @SerializedName("portfolio")
    @Expose
    private List<Portfolio> portfolio = new ArrayList<Portfolio>();

    /**
     *
     * @return
     * The portfolio
     */
    public List<Portfolio> getPortfolio() {
        return portfolio;
    }

    /**
     *
     * @param portfolio
     * The portfolio
     */
    public void setPortfolio(List<Portfolio> portfolio) {
        this.portfolio = portfolio;
    }
}