package com.example.jonathan.myapplication.interfaces;

/**
 * Created by jonathan.asilo on 9/16/2016.
 */
public interface LoadFragment {

   void onLoadFragment();

}
