package com.example.jonathan.myapplication.helpers;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.Toolbar;

import com.example.jonathan.myapplication.R;

/**
 * Created by thor on 16/10/16.
 */
public class ToolBarHelper {

    private Context mContext;

    public ToolBarHelper(Context context){
        this.mContext = context;
    }

    public Toolbar initToolBar(Toolbar toolbar, String title){

        toolbar.setTitle(title);
        toolbar.setNavigationIcon(R.drawable.ic_back_icon);
        toolbar.setTitleTextColor(Color.WHITE);

        return toolbar;
    }

}
