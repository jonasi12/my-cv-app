package com.example.jonathan.myapplication.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.jonathan.myapplication.R;
import com.example.jonathan.myapplication.models.Portfolio;

import org.w3c.dom.Text;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by jonathan.asilo on 9/16/2016.
 */

public class PortfolioListAdapter extends RecyclerView.Adapter<PortfolioListAdapter.PortfolioListHolder>{

    List<Portfolio> portfolioList;

    public PortfolioListAdapter(List<Portfolio> portfolioList){
        this.portfolioList = portfolioList;
    }

    @Override
    public PortfolioListHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.container_portfolio_data, parent, false);
        return new PortfolioListHolder(view);
    }

    @Override
    public void onBindViewHolder(PortfolioListHolder holder, int position) {
        holder.ivPortfolio.setImageBitmap(portfolioList.get(position).getImage());
        holder.tvTitle.setText(portfolioList.get(position).getTitle());
        holder.tvDesc.setText(portfolioList.get(position).getDescription());
    }

    @Override
    public int getItemCount() {
        return portfolioList.size();
    }

    public static class PortfolioListHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.tvTitle)
        TextView tvTitle;

        @BindView(R.id.tvDesc)
        TextView tvDesc;

        @BindView(R.id.ivPortfolio)
        ImageView ivPortfolio;

        public PortfolioListHolder(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
        }
    }
}
